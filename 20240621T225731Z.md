(Verse 1)
Down in the deep where the shadows play,
We are the dwarves that delve, rompity tombo!
Chinking away at the rocks all day,
With our hammers and picks, oh how they go!

(Chorus)
We are the dwarves that delve, rompity tombo!
Finding treasures untold, oh don't you know?
Plugeons and twibbles, snootles galore,
Hickblicks and bobsules, mipplegorlies more!

(Verse 2)
Through caverns wide where echoes sing,
We are the dwarves that delve, rompity tombo!
In every strike a new gem we bring,
From ancient stone to daylight's glow.

(Chorus)
We are the dwarves that delve, rompity tombo!
Finding treasures untold, oh don't you know?
Plugeons and twibbles, snootles galore,
Hickblicks and bobsules, mipplegorlies more!

(Bridge)
Oh chidwunges bright with a gleam so rare,
Hidden in darkness but we find them there.
With laughter and song our spirits soar high,
For deep in the earth our fortunes lie.

(Verse 3)
By lantern light or by fire's warm glow,
We are the dwarves that delve, rompity tombo!
Through tunnels winding where secrets flow,
Our hearts beat strong as we dig below.

(Chorus)
We are the dwarves that delve, rompity tombo!
Finding treasures untold, oh don't you know?
Plugeons and twibbles, snootles galore,
Hickblicks and bobsules, mipplegorlies more!

(Finale)
So raise your mugs to a miner’s cheer,
For we are the dwarves who hold no fear.
With plugeons bright and chidwunges near,
Rompity tombo! Our path is clear!

(Pof pof pof!)

# Fri 21 Jun 22:57:31 CEST 2024 - Write me a song from Tolkien entitled, "We are the dwarves that delve (rompity tombo!)", which has a chorus featuring the title as a line. The song should be in Tom Bombadil style (even you can do that), and should talk about how the dwarves chink away at their rocks and find plugeons, twibbles, snootles, hickblicks, bobsules, mipplegorlies, and chidwunges. Have at! Pof pof pof.